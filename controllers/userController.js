const bcrypt = require("bcrypt");
const Product = require('../models/Product');
const User = require('../models/User');
const auth = require("../auth");
const Order = require('../models/Order');




module.exports.checkEmailExists = (reqBody) => {

  
  return User.find({ email : reqBody.email }).then(result => {

    
    if(result.length > 0) {

      return true;
    
    } else {

      return false;
    }
  }).catch(err => err);
};


module.exports.registerUser = (reqBody) => {

  
  let newUser = new User({
    email: reqBody.email,
    mobileNo : reqBody.mobileNo,
    password : bcrypt.hashSync(reqBody.password, 10)
  });

  return newUser.save().then(user => {

    if(user){
      return true
    } else {

      return false
    }
  }).catch(err => err)
};

// ============= User Authentication ===============
module.exports.loginUser = (reqBody) => {

  return User.findOne({ email: reqBody.email })
    .then(result => {
      if (result == null) {
        return { error: 'User not found' };
      } else {
        const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
        if (isPasswordCorrect) {
          return { access: auth.createAccessToken(result) };
        } else {
          return { error: 'Invalid password' };
        }
      }
    })
    .catch(err => {
      console.log(err);
      err.message = 'Error in login User';
      return { error: err.message };
    });
};



// ================= Retrieve User Details =============


module.exports.getUser = (data) => {

  return User.findById(data.userId)

    .select("-orderedProducts -password") // exluding orderedProducts and password
    .then(result => { return result; }).catch(err => res.send(err));
};


// ==================== SET AS ADMIN =======================

module.exports.setAsAdmin = async function(req, res) {
  const userId = req.params.userId;
  const user = await User.findByIdAndUpdate(
    userId,
    { isAdmin: true },
    { new: true }
  );

  if (!user) {
    return res.status(404).json({ message: 'User not found' });
  }

  return res.json({ message: 'User updated successfully' });
};



// ============== cart ==============


module.exports.getOrder = async (data) => {
  try {
    const orders = await Order.find({ userId: data.userId });
    const formattedOrders = orders.map((order) => {
      const { products, totalAmount } = order;
      const formattedProducts = products.map((item) => ({
        _id: item.productId,
        name: item.productName,
        price: item.price,
        quantity: item.quantity,
      }));
      return { formattedProducts, totalAmount };
    });
    return formattedOrders;
  } catch (err) {
    throw err;
  }
};

// =============== All Users ===================

module.exports.getAllUsers = () => {

  return User.find({}).then(result => result)
    .catch(err => err);
}


// ====================== all orders =================


module.exports.getAllOrders = () => {

  return Order.find({}).then(result => { 

    return result
    console.log(result)
  })
    .catch(err => err);
}
