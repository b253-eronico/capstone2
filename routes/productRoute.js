const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/productController");
const cors = require("cors")



// =========== Create Product Admin Only ===============
router.post("/create", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    

    productController.createProduct(req.body).then((resultFromController) => {
      res.send(resultFromController);
      })
      .catch((err) => {

        res.status(500).send({ error: err.message });
        
      });

  } else {

    res.send(false);

  }
});



// ================ GET ALL PRODUCTS ==================
router.get("/all", (req, res) =>{

  productController.getAllProducts(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
})


// ======= GET ALL ACTIVE PRODUCTS =====================
router.get("/active", (req, res) =>{

  productController.getAllActiveProducts(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
})


module.exports = router;

// ================= GET SPECIFIC PRODUCTS ============
router.get("/:productId", (req, res) => {

  productController.getProduct(req.params).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});


// ================= Update A product ===================
router.put("/:productId/update", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);

  if (userData.isAdmin) {
    productController
      .updateProduct(req.params, req.body)
      .then((success) => {
        // Check if the update operation was successful
        if (success) {
          res.send(true);
        } else {
          res.send(err);
        }
      })
      .catch((err) => res.send(err));
  } else {
    return res.send(err);
  }
});

// ============= Archiving Product =====================
router.patch("/:productId/status", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin){

    productController.statusProduct(req.params).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

  } else {

    res.send(false);

  }
});

// ================ Activating a Product ====================
router.patch("/:productId/activate", auth.verify, (req, res) => {

  const userData = auth.decode(req.headers.authorization);

  if(userData.isAdmin){

    productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

  } else {

    res.send(false);

  }
});

// ============ order =================
router.post('/:productId/order', async (req, res) => {
    try {
        const resultFromController = await productController.test(req.body, res);
        res.send(resultFromController);
    } catch (err) {
        console.log(err);
        res.send(err)
    }
});





module.exports = router;