const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors")

const app = express();
const port = 2000
const db = mongoose.connection

mongoose.connect("mongodb+srv://admin:admin123@batch253-eronico.6rytlt2.mongodb.net/e-CommerceAPI?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
});

const userRoute = require("./routes/userRoute");
const productsRoute = require("./routes/productRoute");


db.once("open", () => console.log("Now connected to MongoDB Atlas."));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors())

app.use("/users", userRoute);
app.use("/products", productsRoute);



if(require.main === module ){
	app.listen(port, () => {
		console.log(`API is now online on port ${port}`)
	})
}

module.exports = app